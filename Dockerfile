FROM python:3.6-slim-stretch

COPY . .

RUN pip install -r requirements.txt

CMD ["flask", "main.py"]